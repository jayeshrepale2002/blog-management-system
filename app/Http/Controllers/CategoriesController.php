<?php

namespace App\Http\Controllers;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware(['validateCategoryTagAuthor'])->only(['create', 'edit', 'update', 'destroy']);
    }

    public function index()
    {
        // $categories = Category::orderBy('id', 'desc')->paginate(2);
        $categories = Category::withCount('blogs')->latest('updated_at')->paginate(3);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCategoryRequest $request)
    {
        // $data = request()->all();
        // request()->validate([
        //     'name' => 'required|min:3|max:255'
        // ]);
        $userId = auth()->user()->id;

        Category::create([
            // dd($request),
            'name' => $request->name,
            'created_by' => $userId,
            'last_updated_by' => $userId
        ]);

        // session()->put('success', 'Category Created Successfully......');
        session()->flash('success', 'Category created successfully.....');
        return redirect(route('admin.categories.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)  // Ye parameter matlab Routing Modal binding hai matlab jo humein update karna hai category uska id apne aap  internally url me paas karta hai apne ko khud se pass karne ki jarurat nahi padti
    {
        return view('admin.categories.edit', compact(['category']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category) // Ye parameter matlab Routing Modal binding hai matlab jo humein update karna hai category uska id apne aap  internally url me paas karta hai apne ko khud se pass karne ki jarurat nahi padti
    {
        $category->name = $request->name;
        $category->last_updated_by = auth()->user()->id;
        $category->save();

        session()->flash('success', 'Category updated successfully.....');
        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Category $category)
    {
        // TODO: Validate whether the category has post associated with it. If not then only proceed.
        if($category->blogs->count() > 0) {
            session()->flash('error', 'Category cannot be deleted as it has posts associated.');
            return redirect(route('admin.categories.index'));
        }
        $category->delete();

        session()->flash('success', 'Category deleted successfully.....');
        return redirect(route('admin.categories.index'));
    }
}
