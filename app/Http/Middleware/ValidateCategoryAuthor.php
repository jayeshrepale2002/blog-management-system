<?php

namespace App\Http\Middleware;

use App\Models\Category;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateCategoryAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // if(!is_object($request->category)) {
        //     $category = Category::find($request->category);
        // } else {
        //     $category = $request->category;
        // }

        if(!auth()->user()->isAdmin()) {
            return abort(401);
        }
        return $next($request);
    }
}
