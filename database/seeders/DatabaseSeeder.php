<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            "name" => 'Jayesh',
            "email" => 'jayeshrepale2002@gmail.com',
            "password" => Hash::make('Jayesh1234'),
            'role' => 'admin'
        ]);
        \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $categories = ['Sports', 'Technology', 'Gaming'];

        foreach($categories as $category) {
            $user = User::all()->random();
            Category::create([
                'name' => $category,
                'created_by' => $user->id,
                'last_updated_by' => $user->id
            ]);
        }

        $tags = ['Book', 'History', 'World War'];

        foreach($tags as $tag) {
            Tag::create(['name' => $tag]);
        }

        $this->call(BlogsSeeder::class); // Here we are calling BlogsSeeder
    }
}
